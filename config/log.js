/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * https://sailsjs.com/docs/concepts/logging
 */

const winston = require('winston');
const winstonLoggly = require('winston-loggly-bulk');

const sailsLevels = {
  levels: {
    silly: 7,
    verbose: 6,
    info: 5,
    debug: 4,
    warn: 3,
    error: 2,
    crit: 1,
    silent: 0
  },
  colors: {
    silly:   'black',
    verbose: 'dimgray',
    info:    'green',
    warn:    'yellow',
    debug:   'blue',
    error:   'red',
    crit:    'bgRed'
  }
};

const customLogger = new winston.Logger({
  levels: sailsLevels.levels,
  transports: [
    new winston.transports.Console({
      colorize: true
    }),
  ],
});

if(process.env.NODE_ENV === 'production'){
  console.debug('Logging -> Using Loggly');
  customLogger.add(
    winstonLoggly.Loggly, {
      token: 'f08f614a-da7e-45ad-9081-63bdfc41b901',
      subdomain: 'dribba',
      tags: ['DuDaGroup'],
      json:true
    });
}

module.exports.log = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/

  // level: 'info'
  inspect: false,
  custom: customLogger

};
