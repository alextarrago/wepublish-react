/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'pages/homepage',
    locals: {
      layout: null
    }
  },
  '/tsuri': {
    view: 'pages/tsuri',
    locals: {
      layout: null
    }
  },
  '/infosperber': {
    view: 'pages/infosperber',
    locals: {
      layout: null
    }
  },
  '/zentralplus': {
    view: 'pages/zentralplus',
    locals: {
      layout: null
    }
  },

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  'POST /login$': {controller: 'WalletController', action: 'login', skipAssets: true},
  'POST /signup$': {controller: 'WalletController', action: 'createWallet', skipAssets: true},
  'GET /status$': {controller: 'WalletController', action: 'getWallet', skipAssets: true},

  'POST /execute$': {controller: 'TransactionController', action: 'executeTransaction', skipAssets: true},


};
