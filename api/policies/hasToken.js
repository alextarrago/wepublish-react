
const jwt = require('jsonwebtoken');

module.exports = async function (req, res, proceed) {

  if (req.headers.authorization && req.headers.authorization.length>8) {

    let token = req.headers.authorization.substring(7, req.headers.authorization.length);

    jwt.verify(token,
      sails.config.custom.jwtSecret, function(err, decoded) {
      if (err) {
        err.from = 'hasToken';
        return res.fail('Action forbidden.', 403);
      }
      req.me = decoded;
      return proceed();
    });

  }else {

    return res.fail('Action forbidden.', 403);

  }

};
