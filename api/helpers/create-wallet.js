
const uuidv4 = require('uuid/v4');

module.exports = {


  friendlyName: 'Create wallet',


  description: '',


  inputs: {

    password: {
      type: 'string',
      description: 'Wallet password.',
      required: true
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    let id = uuidv4();

    while (await Wallet.count({id: id})!==0) {
      id = uuidv4();
    }

    let wallet = await Wallet.create({id: id}).fetch();

    // All done.
    return exits.success(wallet);

  }


};

