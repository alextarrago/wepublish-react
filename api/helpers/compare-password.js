
let bcrypt = require('bcrypt');

module.exports = {


  friendlyName: 'Compare password',


  description: 'Compare password string with hash.',


  inputs: {

    password: {
      type: 'string',
      description: 'Password string.',
      required: true
    },

    hash: {
      type: 'string',
      description: 'Password hash.',
      required: true
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    bcrypt.compare(inputs.password, inputs.hash, (err, result) => {
      if (err) {return exits.error(err);}
      if (!result) {return exits.success(false);}
      return exits.success(true);
    });

  }


};

