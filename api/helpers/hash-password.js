
let bcrypt = require('bcrypt');

module.exports = {


  friendlyName: 'Hash password',


  description: 'Hashes passwords in form of string.',


  inputs: {

    password: {
      type: 'string',
      description: 'Password string.',
      required: true
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return exits.error(err);
      }else {
        bcrypt.hash(inputs.password, salt, (err, hash) => {
          if (err) {
            return exits.error(err);
          } else {
            return exits.success(hash);
          }
        });
      }
    });

  }


};

