/**
 * WalletController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const jwt = require('jsonwebtoken');

module.exports = {

  login: async function(req, res) {

    let id = req.param('id');
    let password = req.param('password');

    try {
      let wallet = await Wallet.findOne({id: id});
      if (!wallet) {return res.fail('Wallet not found.', 404);}

      if (await sails.helpers.comparePassword(password, wallet.password)) {
        let token = jwt.sign({
          wallet: wallet.id,
        }, sails.config.custom.jwtSecret, { expiresIn: '1h' });

        return res.result({
          token: token,
        });
      }else {
        return res.fail('Wallet not found.');
      }
    }catch (err) {
      err.from = 'WalletController.login';
      return res.fail('Error.', 418, err);
    }

  },

  createWallet: async function(req, res) {

    let password = req.param('password');

    try {

      if (!password) {return res.fail('Bad request.');}

      let wallet = await sails.helpers.createWallet(password);

      let token = jwt.sign({
        wallet: wallet.id,
      }, sails.config.custom.jwtSecret, { expiresIn: '1h' });

      return res.result({
        token: token,
        coins: wallet.coins
      });

    }catch (err) {
      err.from = 'WalletController.createWallet';
      return res.fail('Error.', 418, err);
    }

  },


  getWallet: async function(req, res) {

    try {

      let wallet = await Wallet.findOne({id: req.me.wallet});

      return res.result(wallet);

    }catch (err) {
      err.from = 'WalletController.getWallet';
      return res.fail('Error.', 418, err);
    }

  },

};

