/**
 * TransactionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  executeTransaction: async function(req, res) {

    let toWalletId = req.param('to_wallet');
    let coins = parseInt(req.param('coins'));

    try {

      if (!toWalletId) {return res.fail('Bad request.');}
      let toWallet = await Wallet.findOne({id: toWalletId});
      if (!toWallet) {return res.fail('Wallet not found.', 404);}

      let fromWallet = await Wallet.findOne({id: req.me.wallet});

      if (fromWallet.coins>=coins) {
        return res.fail('Not enough founds.', 400);
      }

      await Wallet.update({id: toWallet.id}, {coins: toWallet.coins+coins});
      await Wallet.update({id: fromWallet.id}, {coins: fromWallet.coins-coins});
      await Transaction.create({from: fromWallet, to: toWallet, coins: coins});

      return res.result(wallet);

    }catch (err) {
      err.from = 'TransactionController.executeTransaction';
      return res.fail('Error.', 418, err);
    }

  },

};

