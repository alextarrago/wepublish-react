/**
 * Wallet.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  primaryKey: 'id',

  attributes: {

    id: {
      type: 'string',
      unique: true,
      required: true,
    },
    password: {
      type: 'string',
      required: true,
    },
    coins: {
      type: 'number',
      defaultsTo: 100,
    }

  },
  beforeCreate: async function(object, cb) {
    if (object.password) {
      object.password = await sails.helpers.hashPassword(object.password);
      return cb();
    }else {
      return cb();
    }
  },
  beforeUpdate: async function(object, cb) {
    if (object.password) {
      object.password = await sails.helpers.hashPassword(object.password);
      return cb();
    }else {
      return cb();
    }
  },

};

