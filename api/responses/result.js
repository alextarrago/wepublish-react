/**
 * result.js
 *
 * A success return of data.
 *
 * Example usage:
 * ```
 *     return res.result();
 *     // -or-
 *     return res.result(data, message);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'result'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */

module.exports = function result(data, message) {

  // Get access to `req` and `res`
  // let req = this.req;
  let res = this.res;

  let statusCodeToSet = 200;

  let result = {
    success: true,
    message: 'OK'
  };

  if (message) {
    result.message = message;
  }

  if (data) {
    result.data = data;
  }

  return res.status(statusCodeToSet).json(result);

};
