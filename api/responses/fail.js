/**
 * fail.js
 *
 * A custom response.
 *
 * Example usage:
 * ```
 *     return res.fail();
 *     // -or-
 *     return res.fail(optionalData);
 * ```
 *
 * Or with actions2:
 * ```
 *     exits: {
 *       somethingHappened: {
 *         responseType: 'fail'
 *       }
 *     }
 * ```
 *
 * ```
 *     throw 'somethingHappened';
 *     // -or-
 *     throw { somethingHappened: optionalData }
 * ```
 */

module.exports = function fail(message, code, err) {

  // Get access to `req` and `res`
  let req = this.req;
  let res = this.res;

  // Define the status code to send in the response.
  let statusCodeToSet = 400;

  let result = {
    success: false,
    message: 'OK'
  };

  if (message) {
    result.message = message;
  }

  if (code) {
    statusCodeToSet = code;
  }

  if (err) {
    sails.log.error(err.from, err);
  }

  return res.status(statusCodeToSet).json(result);

};
