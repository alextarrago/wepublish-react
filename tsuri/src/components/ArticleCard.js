import React from "react";
import {
  Card,
  CardBody,
  CardImgOverlay,
  Button,
  CardImg,
  CardText,
  CardTitle } from 'reactstrap';
import './ArticleCard.css';

class ArticleCard extends React.Component {

  onBtnClick(url) {
    window.open(url, '_blank');
  }

  render() {
    return (
      <Card className="h-100">
        <CardImg top width="100%" src={this.props.imageUrl || "https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" } alt="Article image" />
        {/*<CardImgOverlay>*/}
          {/*<CardTitle>{this.props.title}</CardTitle>*/}
        {/*</CardImgOverlay>*/}
        <CardBody>
          <CardTitle>{this.props.title}</CardTitle>
          {this.props.short_desc &&
            <CardText>{this.props.short_desc}</CardText>
          }
          <CardText>
            <small className="text-muted">{this.props.timestamp}</small>
          </CardText>
          <Button onClick={() => this.onBtnClick(this.props.link)}>View article</Button>
        </CardBody>
      </Card>
    );
  }

}

export default ArticleCard;
