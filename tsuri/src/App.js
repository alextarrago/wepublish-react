import React, { Component } from 'react'
import './App.css';
import CustomNavBar from './components/CustomNavBar'
import ArticleCard from './components/ArticleCard'
import mainBanner from './images/Smart_Tsuri.jpg';

import data from './test_data.json';

import {
  Container,
  Row,
  Col} from 'reactstrap';


class App extends Component {

  render() {
    return (
      <div>
        <CustomNavBar/>
        <Container>
          <Row>
            <Col lg="12" className="mt-3">
              <div className="text-center">
                <img className="App-banner" src={mainBanner} alt="banner"/>
              </div>
            </Col>
          </Row>
          <Row >
            {

              data.map(function(article){
                return (
                  <Col md="4" className="mt-3">
                    <ArticleCard title={article.data.title} short_desc={article.data.description}
                                 timestamp={article.data.published} imageUrl={article.data.image}
                                 link={article.data.link}/>
                  </Col>
                );
              })

            }
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
