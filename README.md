# WePublish

WePublish crypto emulator and webserver.

### Setup info
First time (or whenever a library is missing) execute:  
  
```
npm install
```
  
To start the server: 

```
node app.js
```

### Developer info
Contact: Xavier Bassols  
Company: Dribba GmbH  
Email: [xbassols@dribba.ch](mailto:xbassols@dribba.ch?Subject=DuDaGroup)  
Website: [Dribba](https://www.dribba.ch)


### Version info

Alpha 1.0
